# daily-workload-tracker

This is my personal favourite project. A simple python program that keeps track of my daily workload in a simple .csv database.

This `log.py` can be automate inputing the timestamp, what kind of work (categorical) and description with a single line of code in terminal, to keep track what did you do for a day.

For use, simply download the `log.py` and place it on the root of terminal, it used to be `~/` or `/home/<user>`

The program has a separator with sign of `%` and it has 3 sections:
1. Type of work (categorical), you can specify by yourself. Mine is `WH` for WorkHour, `FT` for freetime, `OT` for overtime
2. Title or subject, I create this simple code to see in a month, or a year, what kind of project I up to. So I write the title with comma seperated
3. Description of your today's work. This type of description can be used to inform your bosses about what did you do today, so your boss would be impressed by how detail you can provide them your work details.

For use, edit the .py and specify the .csv file you want to use, change line 6:

`os.chdir('/home/hawe/Documents/hawe/')`

to your own directory.
Dont forget to install pandas by open the teminal and type

`pip install pandas`

and you can use the code by typing on terminal like this

`echo "WH%I'm doing Python!%Today is python day, and I create this log for myself!" | python log.py`

and you will get feedback `Log done!`
