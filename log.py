import os
import sys
import pandas as pd
from datetime import datetime

os.chdir('/home/hawe/Documents/hawe/')
tables = ['datetime','type','keywords','description']
userInput = sys.stdin.read()
logs = userInput.split("%")
worktype = logs[0].upper()
log = pd.DataFrame([(datetime.now().timestamp()*1000,logs[0],logs[1],logs[2].replace('\n',''))], columns=tables)
log.to_csv('worklog.csv', mode='a', index=False, header=False)
print("Log done!")